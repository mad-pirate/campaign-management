<?php declare(strict_types=1);

namespace App\Account\Infrastructure\Mapping;

use App\Account\Domain\Password;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class PasswordType extends Type
{
    const PASSWORD = 'password';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return Type::getType('string')
            ->getSQLDeclaration($fieldDeclaration, $platform);
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return self::PASSWORD;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new Password($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var Password $value */
        return (string) $value;
    }
}
