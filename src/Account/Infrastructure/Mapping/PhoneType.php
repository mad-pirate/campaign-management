<?php declare(strict_types=1);

namespace App\Account\Infrastructure\Mapping;

use App\Account\Domain\Phone;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class PhoneType extends Type
{
    const PHONE = 'phone';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return Type::getType('string')
            ->getSQLDeclaration($fieldDeclaration, $platform);
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return self::PHONE;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new Phone($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var Phone $value */
        return (string) $value;
    }
}
