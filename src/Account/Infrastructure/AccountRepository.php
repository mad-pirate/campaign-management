<?php declare(strict_types=1);

namespace App\Account\Infrastructure;

use App\Account\Domain\Account;
use App\Account\Domain\Repository;
use App\SharedKernel\Library\AggregateId;
use Doctrine\ORM\EntityManagerInterface;

final class AccountRepository implements Repository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function add(Account $account)
    {
        $this->entityManager->persist($account);
    }

    public function fetchById(AggregateId $id): Account
    {
        // TODO: Implement fetchById() method.
    }

    public function generateId(): AggregateId
    {

    }
}
