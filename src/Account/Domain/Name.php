<?php declare(strict_types = 1);

namespace App\Account\Domain;

use App\Account\Exception\Exception;

final class Name
{
    private $name;

    public function __construct($name)
    {
        if (empty($name)) {
            throw new Exception('Name cannot be empty.');
        }

        $this->name = $name;
    }

    public function __toString(): string
    {
        return $this->name;
    }
}
