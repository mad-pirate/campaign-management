<?php declare(strict_types=1);

namespace App\Account\Domain;

use App\SharedKernel\Library\AggregateId;
use App\SharedKernel\Library\Email;

final class Factory
{
    public static function newAccount(
        string $name,
        string $email,
        string $phone,
        string $password
    ): Account {
        return new Account(
            AggregateId::fromString(''),
            new Name($name),
            new Email($email),
            new Phone($phone),
            new Password($password),
            false,
            new \DateTimeImmutable()
        );
    }
}
