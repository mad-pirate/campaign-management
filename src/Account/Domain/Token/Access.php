<?php declare(strict_types=1);

namespace App\Account\Domain\Token;

use App\SharedKernel\Library\AggregateId;

final class Access
{
    /**
     * @var Bearer
     */
    private $accessToken;
    /**
     * @var Bearer
     */
    private $refreshToken;
    /**
     * @var AggregateId
     */
    private $accountId;

    public function __construct(AggregateId $accountId, Bearer $accessToken, Bearer $refreshToken)
    {
        $this->accountId = $accountId;
        $this->accessToken = $accessToken;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return AggregateId
     */
    public function accountId(): AggregateId
    {
        return $this->accountId;
    }

    /**
     * @return Bearer
     */
    public function accessToken(): Bearer
    {
        return $this->accessToken;
    }

    /**
     * @return Bearer
     */
    public function refreshToken(): Bearer
    {
        return $this->refreshToken;
    }
}
