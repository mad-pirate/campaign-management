<?php declare(strict_types=1);

namespace App\Account\Domain\Token;

use App\Account\Exception\Exception;

final class Bearer
{
    /**
     * @var string
     */
    private $token;

    public function __construct(string $token, \DateTimeImmutable $expiresIn)
    {
        if (empty($token)) {
            throw new Exception('The token cannot be empty.');
        }
        $this->token = $token;
    }

    public function __toString(): string
    {
        return $this->token;
    }
}
