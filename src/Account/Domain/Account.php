<?php declare(strict_types = 1);

namespace App\Account\Domain;

use App\SharedKernel\Library\AggregateId;
use App\SharedKernel\Library\Email;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Account\Domain\Repository")
 *
 */
final class Account
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;
    /**
     * @ORM\Column(type="name", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="email", length=255)
     */
    private $email;
    /**
     * @ORM\Column(type="phone", length=32)
     */
    private $phone;
    /**
     * @ORM\Column(type="password", length=64)
     */
    private $password;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isAdmin;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct(
        AggregateId $id,
        Name $name,
        Email $email,
        Phone $phone,
        Password $password,
        bool $isAdmin,
        \DateTimeInterface $createdAt
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->password = $password;
        $this->isAdmin = $isAdmin;
        $this->createdAt = $createdAt;
    }
}
