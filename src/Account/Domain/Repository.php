<?php declare(strict_types=1);


namespace App\Account\Domain;


use App\SharedKernel\Library\AggregateId;

interface Repository
{
    public function add(Account $account);
    public function fetchById(AggregateId $id): Account;
    public function generateId(): AggregateId;
}
