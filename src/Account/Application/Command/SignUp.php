<?php declare(strict_types=1);

namespace App\Account\Application\Command;

use App\SharedKernel\Library\Request;

final class SignUp
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function name(): string
    {
        return (string) $this->request->get('name');
    }

    public function email(): string
    {
        return (string) $this->request->get('email');
    }

    public function phone(): string
    {
        return (string) $this->request->get('phone');
    }

    public function password(): string
    {
        return (string) $this->request->get('password');
    }
}
