<?php declare(strict_types=1);

namespace App\Account\Application\Command;

use App\SharedKernel\Library\Request;

final class Login
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function email(): string
    {
        return (string) $this->request->get('username');
    }

    public function passwordHash(): string
    {
        return (string) $this->request->get('password');
    }
}
