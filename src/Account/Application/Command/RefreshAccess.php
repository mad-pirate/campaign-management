<?php declare(strict_types=1);

namespace App\Account\Application\Command;

use App\SharedKernel\Library\Request;

final class RefreshAccess
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function refreshToken(): string
    {
        return (string) $this->request->get('refresh_token');
    }
}
