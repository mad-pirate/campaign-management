<?php declare(strict_types=1);

namespace App\Account\Application\Command;

use App\SharedKernel\Library\Request;

final class RevokeAccess
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function accessToken(): string
    {
        return (string) $this->request->get('access_token');
    }
}
