<?php declare(strict_types=1);

namespace App\Account\Application\Handler;

use App\Account\Application\Command\SignUp as Command;
use App\Account\Domain\Repository as AccountRepository;
use App\Account\Domain\Factory as AccountFactory;
use App\Account\Exception\Exception;
use App\SharedKernel\Response\Error;
use App\SharedKernel\Response\Success;

final class SignUp
{
    /**
     * @var AccountRepository
     */
    private $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function execute(Command $command)
    {
        try {
            $account = $this->accountRepository->add($this->toAccount($command));

            return new Success([]);

        } catch (Exception $exception) {
            return new Error('101', $exception->getMessage(), []);
        }
    }

    private function toAccount(Command $command)
    {
        return AccountFactory::newAccount(
            $command->name(),
            $command->email(),
            $command->phone(),
            $command->password()
        );
    }
}
