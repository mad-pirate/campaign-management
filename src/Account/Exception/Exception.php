<?php declare(strict_types = 1);

namespace App\Account\Exception;

final class Exception extends \Exception
{
}
