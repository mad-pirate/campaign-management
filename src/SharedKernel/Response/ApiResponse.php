<?php declare(strict_types=1);

namespace App\SharedKernel\Response;


interface ApiResponse
{
    public function httpCode(): int;
    public function payload(): \stdClass;
}
