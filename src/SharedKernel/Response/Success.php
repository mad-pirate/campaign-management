<?php declare(strict_types=1);

namespace App\SharedKernel\Response;

final class Success implements ApiResponse
{
    const HTTP_SUCCESS_CODE = 200;
    const STATUS_CODE = '212';

    /**
     * @var
     */
    private $payload;

    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    public function httpCode(): int
    {
        return static::HTTP_SUCCESS_CODE;
    }

    public function payload(): \stdClass
    {
        return (object) [
            'code' => static::STATUS_CODE,
            'payload' => $this->payload,
        ];
    }
}
