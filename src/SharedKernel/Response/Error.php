<?php declare(strict_types=1);

namespace App\SharedKernel\Response;

final class Error implements ApiResponse
{
    const HTTP_BAD_REQUEST_CODE = 400;

    /**
     * @var
     */
    private $payload;
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $message;

    public function __construct(string $code, string $message, $payload)
    {
        $this->code = $code;
        $this->message = $message;
        $this->payload = $payload;
    }

    public function httpCode(): int
    {
        return static::HTTP_BAD_REQUEST_CODE;
    }

    public function payload(): \stdClass
    {
        return (object) [
            'code' => $this->code,
            'message' => $this->message,
            'payload' => $this->payload,
        ];
    }
}
