<?php declare(strict_types=1);

namespace App\SharedKernel\Infrastructure\Mapping;

use App\SharedKernel\Library\AggregateId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class AggregateIdType extends Type
{
    const AGGREGATE_ID = 'aggregate_id';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return Type::getType('string')
            ->getSQLDeclaration($fieldDeclaration, $platform);
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return self::AGGREGATE_ID;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return AggregateId::fromString($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var AggregateId $value */
        return (string) $value;
    }
}
