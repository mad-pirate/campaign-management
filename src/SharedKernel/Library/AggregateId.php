<?php declare(strict_types=1);

namespace App\SharedKernel\Library;

final class AggregateId
{
    /**
     * @var string
     */
    private $id;

    private function __construct(string $id)
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    public static function fromString(string $id): AggregateId
    {
        return new self($id);
    }
}