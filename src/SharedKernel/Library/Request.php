<?php declare(strict_types=1);

namespace App\SharedKernel\Library;

final class Request
{
    const UNDEFINED = '';

    /**
     * @var array
     */
    private $parameters;

    public function __construct(array $parameters)
    {
        $this->parameters = (object) $parameters;
    }

    public function get(string $property)
    {
        return isset($this->parameters->{$property}) ? $this->parameters->{$property} : self::UNDEFINED;
    }
}
