<?php declare(strict_types=1);

namespace App\SharedKernel\Library;

final class Email
{
    /**
     * @var string
     */
    private $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function __toString(): string
    {
        return $this->email;
    }
}
