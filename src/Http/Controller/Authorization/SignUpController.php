<?php declare(strict_types = 1);

namespace App\Http\Controller\Authorization;

use App\Account\Application\Command\SignUp;
use App\Account\Application\Handler\SignUp as SignUpHandler;
use App\SharedKernel\Library\Request as DomainRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class SignUpController extends Controller
{
    public function signUp(Request $request, SignUpHandler $handler): JsonResponse
    {
        $response = $handler->execute(new SignUp($this->convertRequest($request)));

        return new JsonResponse($response->payload(), $response->httpCode());
    }

    private function convertRequest(Request $request): DomainRequest
    {
        return new DomainRequest($request->request->all());
    }
}
