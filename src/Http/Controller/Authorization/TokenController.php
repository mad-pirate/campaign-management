<?php declare(strict_types = 1);

namespace App\Http\Controller\Authorization;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class TokenController extends Controller
{
    const GRANT_TYPE_PASSWORD = 'password';
    const GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';

    public function grant(Request $request) :JsonResponse
    {
        switch ($request->get('grant_type')) {
            case self::GRANT_TYPE_PASSWORD:
                return $this->password($request);
            case self::GRANT_TYPE_REFRESH_TOKEN:
                return $this->refresh($request);
            default:
                return $this->unknownGrantType();
        }
    }

    public function password(Request $request) :JsonResponse
    {
        return new JsonResponse([
            'access_token' => md5(microtime()),
            'refresh_token' => md5(microtime()),
            'scope' => 'default',
            'expires_in' => '3600',
        ]);
    }


    public function refresh(Request $request) :JsonResponse
    {
        return new JsonResponse([
            'access_token' => md5(microtime()),
            'refresh_token' => md5(microtime()),
            'scope' => 'default',
            'expires_in' => '3600',
        ]);
    }

    private function unknownGrantType() :JsonResponse
    {
        return new JsonResponse(['Unsupported Grant Type'], Response::HTTP_BAD_REQUEST);
    }
}
