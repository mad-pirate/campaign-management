<?php declare(strict_types = 1);

namespace App\Http\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller {

    public function index()
    {
        return $this->render('base.html.twig');
    }
}